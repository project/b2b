
-- SUMMARY --

Integration module with OroCommerce Application (https://oroinc.com/orocommerce).

For a full description of the module, visit the project page:
  http://drupal.org/project/b2b
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/b2b

-- REQUIREMENTS --

* orocommerce
  https://github.com/oroinc/orocommerce

-- INSTALLATION --

1. Execute 'composer update' from the site's root directory (this will download
   the orocommerce).

2. Install the module as usual, see http://drupal.org/node/70151 for further
   information.

-- CONFIGURATION --

* Configure user permissions at Administer >> User management >> Access
  control >> b2b module.

  Only users with the "administer b2b settings" permission are allowed to
  access the module configuration page.

* Enable the B2B module at Administer >> Site
  configuration >> b2b.

  -- USAGE --

* After the module configuration you can use the OroCommerce Application in your custom modules.

-- CREDITS --

Authors:
* Eleo Basili (eleonel) - http://drupal.org/u/eleonel

This project has been sponsored by Spinetta (http://spinetta.tech).
