<?php

namespace Drupal\b2b\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures b2b settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   */
  public function __construct(LanguageManagerInterface $language_manager) {
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'b2b_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['b2b.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('b2b.settings');

    $form['b2b_on'] = [
      '#type' => 'checkbox',
      '#title' => t('Activate B2B.'),
      '#default_value' => $config->get('b2b_on'),
      '#description' => t('Enable OroCommerce https://oroinc.com/orocommerce'),
    ];

    if (!$config->get('b2b_on') && empty($form_state->input)) {
      drupal_set_message(t('b2b is currently disabled.'), 'warning');
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    // Check to see if the module has been activated or inactivated.
    if ($values['b2b_on']) {
      if (!b2b_active()) {
        drupal_set_message(t('B2B OroCommerce is ready to use in your modules.'));
        \Drupal::logger('b2b')->notice('b2b has been enabled.');
      }
    }
    elseif (b2b_active()) {
      // This module is active and is being inactivated.
      drupal_set_message(t('b2b has been disabled.'));
      \Drupal::logger('b2b')->notice('b2b has been disabled.');
    }

    // Save the configuration changes.
    $b2b_config = $this->config('b2b.settings');
    $b2b_config->set('b2b_on', $values['b2b_on']);

    $b2b_config->save();

    parent::submitForm($form, $form_state);
  }

}
